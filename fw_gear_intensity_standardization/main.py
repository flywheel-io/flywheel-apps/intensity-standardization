"""Main module."""

import logging
import os

from nipype.interfaces.slicer.filtering import HistogramMatching

log = logging.getLogger(__name__)


def run(src_image, ref_image, hist_levels, match_points, thresh, output_dir):

    """This gear normalizes the grayscale values of a source image based on the grayscale values of a reference image.
    This filter uses a histogram matching technique where the histograms of the two images are matched only at a
    specified number of quantile values.

    The filter was originally designed to normalize MR images of the same MR
    protocol and same body part. The algorithm works best if background pixels are excluded from both the source and
    reference histograms. A simple background exclusion method is to exclude all pixels whose grayscale values are
    smaller than the mean grayscale value. ThresholdAtMeanIntensity switches on this simple background exclusion method.

    Number of match points governs the number of quantile values to be matched. The filter assumes that both the source
    and reference are of the same type and that the input and output image type have the same number of dimension and
    have scalar pixel types.

    It is recommended that this gear should be applied after the Gradient Anisotropic Diffusion Denoising gear. If bias
    correction is the final operation performed on the scene, it will likely introduce some non-standardness.


    Args:
        src_image: A nifti file.
        ref_image: A nifti file.
        hist_levels: The number of histogram levels to use.
        match_points: The number of match points to use.
        thresh: If true, only pixels above the mean in each volume are thresholded.
        output_dir: Directory of matched output file.

    Returns:
        (int): Status code for HistogramMatching workflow.
    """

    log.info("Starting the process of Histogram Matching Intensity Standardization...")
    hist = HistogramMatching()
    hist.inputs.inputVolume = src_image
    hist.inputs.referenceVolume = ref_image
    hist.inputs.numberOfHistogramLevels = hist_levels
    hist.inputs.numberOfMatchPoints = match_points
    hist.inputs.threshold = thresh

    src_image = str(src_image)
    test = src_image.split(".")
    if test[-1] == "gz":
        file_name = src_image.split(".nii.gz")
        file_name = file_name[0].split("/")
        file_name = file_name[-1] + "_matched.nii.gz"
    elif test[-1] == "nii":
        file_name = src_image.split(".nii")
        file_name = file_name[0].split("/")
        file_name = file_name[-1] + "_matched.nii"

    hist.inputs.outputVolume = str(output_dir / file_name)

    hist.run()

    return 0
