"""Module to test main.py"""

import os

import pytest

from fw_gear_intensity_standardization.main import run


def test_run(tmp_path, mocker):
    src_image = tmp_path / "test_file_src.nii.gz"
    src_image.touch()
    ref_image = tmp_path / "test_file_ref.nii.gz"
    ref_image.touch()
    hist_levels = 256
    match_points = 20
    thresh = "false"

    out_dir = tmp_path / "output"
    int_stand = mocker.patch("fw_gear_intensity_standardization.main.HistogramMatching")

    e_code = run(src_image, ref_image, hist_levels, match_points, thresh, out_dir)

    head_tail = os.path.split(src_image)

    assert e_code == 0
    assert int_stand.return_value.inputs.inputVolume == src_image
    assert int_stand.return_value.inputs.referenceVolume == ref_image
    assert int_stand.return_value.inputs.numberOfHistogramLevels == hist_levels
    assert int_stand.return_value.inputs.numberOfMatchPoints == match_points
    assert int_stand.return_value.inputs.threshold == thresh

    # head_tail = head_tail[1].split(".nii.gz")
    # head_tail = head_tail[0] + "_matched.nii.gz"

    test = head_tail[1].split(".")
    if test[-1] == "gz":
        head_tail = head_tail[1].split(".nii.gz")
        head_tail = head_tail[0].split("/")
        head_tail = head_tail[-1] + "_matched.nii.gz"
    elif test[-1] == "nii":
        head_tail = head_tail[1].split(".nii")
        head_tail = head_tail[0].split("/")
        head_tail = head_tail[-1] + "_matched.nii"

    # print(head_tail)
    # assert int_stand.return_value.inputs.outputVolume == str(out_dir) + "/" + "matched_" + str(head_tail[1])

    assert int_stand.return_value.inputs.outputVolume == str(out_dir) + "/" + head_tail

    int_stand.return_value.run.assert_called_once()
