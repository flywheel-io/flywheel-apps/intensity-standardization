import shutil
from pathlib import Path

import nibabel
import numpy
import pytest

from fw_gear_intensity_standardization.main import run

INPUT_PATH = Path(__file__).parents[0] / "assets/input-src"
INPUT_PATH = Path(__file__).parents[0] / "assets/input-ref"
OUTPUT_PATH = Path(__file__).parents[0] / "assets/output"


@pytest.mark.skip
@pytest.mark.parametrize(
    "hist_levels, match_points",
    [
        (256, 20),
        (512, 40),
        (1024, 50),
    ],
)
def test_integration(tmp_path, hist_levels, match_points):
    src_image = INPUT_PATH / "3_Accelerated_Sagittal_IR_FSPGR.nii"
    ref_image = INPUT_PATH / "3_Accelerated_Sagittal_IR_FSPGR2.nii"
    thresh = "false"
    out_dir = tmp_path / "output"
    e_code = run(src_image, ref_image, hist_levels, match_points, thresh, out_dir)

    out = nibabel.load(out_dir / "matched_image.nii")
    expected_output = nibabel.load(
        OUTPUT_PATH
        / f"hist_levels_hist_levels-{hist_levels}_match_points-{match_points}.nii"
    )
    # Assert expected output with whatever metric.
    assert numpy.all(out.get_fdata() == expected_output.get_fdata())
