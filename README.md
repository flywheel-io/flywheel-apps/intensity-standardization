# Histogram Matching Intensity Standardization Gear

This gear normalizes the grayscale values of a source image based on the grayscale values of a reference image. This filter uses a histogram matching technique where the histograms of the two images are matched only at a specified number of quantile values.

The filter was originally designed to normalize MR images of the same MR protocol and same body part. The algorithm works best if background pixels are excluded from both the source and reference histograms. A simple background exclusion method is to exclude all pixels whose grayscale values are smaller than the mean grayscale value. ThresholdAtMeanIntensity switches on this simple background exclusion method.

Number of match points governs the number of quantile values to be matched.

The filter assumes that both the source and reference are of the same type and that the input and output image type have the same number of dimension and have scalar pixel types.

It is recommended that this gear should be applied after the Gradient Anisotropic Diffusion Denoising gear. If bias correction is the final operation performed on the scene, it will likely introduce some non-standardness.

documentation-url: http://wiki.slicer.org/slicerWiki/index.php/Documentation/4.1/Modules/HistogramMatching 

## Usage

### Inputs

* nifti-input-src: A nifti file for source image. Input volume to be filtered.
* nifti-input-ref: A nifti file for reference image. Input volume whose histogram will be matched.

### Configuration
* hist-levels: (integer, default 256) The number of histogram levels to use.
* match-points: (integer, default 20) The number of match points to use.
* threshold-at-mean: (boolean, default False) If on, only pixels above the mean in each volume are thresholded.


## Contributing

For more information about how to get started contributing to that gear, 
checkout [CONTRIBUTING.md](CONTRIBUTING.md).
